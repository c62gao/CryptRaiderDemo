// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
#include "DrawDebugHelpers.h"
#include "Engine/World.h"
#include "Math/Color.h"

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these
	// features off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	// ...
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	// Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// FRotator rotation = GetComponentRotation();
	// FString rotationString = rotation.ToCompactString();
	// UWorld *world = GetWorld();
	// UE_LOG(LogTemp, Display, TEXT("world time: %f"), world->GetTimeSeconds());
	// UE_LOG(LogTemp, Display, TEXT("Rotation: %s"), *rotationString);
	// ...

	UPhysicsHandleComponent *physicsHandle = GetPhysicsHandle();

	if (physicsHandle && physicsHandle->GrabbedComponent)
	{
		FRotator rotation = GetComponentRotation();

		rotation.Pitch = FMath::Max(0.0f, rotation.Pitch);
		FVector grabPosition = GetComponentLocation() + GetForwardVector() * maxHoldDistance;
		physicsHandle->SetTargetLocationAndRotation(grabPosition, rotation);
	}
}

void UGrabber::Grab()
{
	UPhysicsHandleComponent *physicsHandle = GetPhysicsHandle();
	FHitResult hitResult;
	bool result = GetFirstPhysicsBodyInReach(hitResult);
	if (physicsHandle && result)
	{
		UPrimitiveComponent *hitComponent = hitResult.GetComponent();
		hitResult.GetActor()->Tags.Add("Grabbed");
		hitComponent->SetSimulatePhysics(true);
		hitComponent->WakeAllRigidBodies();
		physicsHandle->GrabComponentAtLocationWithRotation(hitComponent, NAME_None, hitResult.ImpactPoint, GetComponentRotation());
	}
	else
	{
		UE_LOG(LogTemp, Display, TEXT("No hit"));
	}
}

void UGrabber::Release()
{
	UPhysicsHandleComponent *physicsHandle = GetPhysicsHandle();
	if (physicsHandle && physicsHandle->GetGrabbedComponent())
	{
		physicsHandle->GetGrabbedComponent()->GetOwner()->Tags.Remove("Grabbed");
		// Wake all rigid bodies so that they can be affected by gravity
		physicsHandle->GetGrabbedComponent()
			->WakeAllRigidBodies();
		physicsHandle->ReleaseComponent();
	}
}

UPhysicsHandleComponent *UGrabber::GetPhysicsHandle() const
{

	auto result = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	if (result == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("No physics handle component found on %s"), *GetOwner()->GetName());
	}
	return result;
}

bool UGrabber::GetFirstPhysicsBodyInReach(FHitResult &hitResult) const
{
	FVector startLocation = GetComponentLocation();
	FVector endLocation = startLocation + GetForwardVector() * MaxGrabDistance;
	FCollisionShape collisionShape = FCollisionShape::MakeSphere(GrabRadius);
	bool result = GetWorld()->SweepSingleByChannel(hitResult, startLocation, endLocation, FQuat::Identity,
												   ECC_GameTraceChannel2, collisionShape);
	return result;
}
