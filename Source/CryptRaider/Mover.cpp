// Fill out your copyright notice in the Description page of Project Settings.

#include "Mover.h"
#include "Math/UnrealMathUtility.h"

// Sets default values for this component's properties
UMover::UMover()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UMover::BeginPlay()
{
	Super::BeginPlay();
	originalLocation = GetOwner()->GetActorLocation();
	// ...
}

// Called every frame
void UMover::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// AActor *Owner = GetOwner();
	// FString name = Owner->GetActorNameOrLabel();
	// FVector location = Owner->GetActorLocation();
	// FString locationString = location.ToString();
	// UE_LOG(LogTemp, Warning, TEXT("%s is at %s"), *name, *locationString);
	if (shouldMove)
	{
		Move(DeltaTime);
	}
	else
	{
		MoveBack(DeltaTime);
	}
}

void UMover::Move(float DeltaTime)
{
	FVector CurrentLocation = GetOwner()->GetActorLocation();
	FVector TargetLocation = originalLocation + MoveOffset;
	float speed = FVector::Distance(CurrentLocation, TargetLocation) / MoveTime;
	FVector newLocation = FMath::VInterpConstantTo(CurrentLocation, TargetLocation, DeltaTime, speed);
	GetOwner()->SetActorLocation(newLocation);
}

void UMover::MoveBack(float DeltaTime)
{
	FVector CurrentLocation = GetOwner()->GetActorLocation();
	float speed = FVector::Distance(CurrentLocation, originalLocation) / MoveTime;
	FVector newLocation = FMath::VInterpConstantTo(CurrentLocation, originalLocation, DeltaTime, speed);
	GetOwner()->SetActorLocation(newLocation);
}

void UMover::SetShouldMove(bool shouldMoveToSet)
{
	shouldMove = shouldMoveToSet;
}