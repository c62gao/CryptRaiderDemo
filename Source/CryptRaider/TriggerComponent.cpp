// Fill out your copyright notice in the Description page of Project Settings.

#include "TriggerComponent.h"
#include "GameFramework/Actor.h"
#include "Components/PrimitiveComponent.h"
// constructor
UTriggerComponent::UTriggerComponent()
{
    UE_LOG(LogTemp, Warning, TEXT("TriggerComponent Constructor"));
    PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UTriggerComponent::BeginPlay()
{
    Super::BeginPlay();
    UE_LOG(LogTemp, Warning, TEXT("TriggerComponent BeginPlay"));
}

// Called every frame
void UTriggerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
    AActor *Actor = GetAcceptableActor();
    if (Actor && Mover)
    {

        UPrimitiveComponent *RootComponent = Cast<UPrimitiveComponent>(Actor->GetRootComponent());
        if (RootComponent)
        {
            RootComponent->SetSimulatePhysics(false);
            RootComponent->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);
        }
        Mover->SetShouldMove(true);
    }
    else
    {
        if (Mover)
        {
            Mover->SetShouldMove(false);
        }
    }
}

AActor *UTriggerComponent::GetAcceptableActor() const
{
    TArray<AActor *> OverlappingActors;
    GetOverlappingActors(OverlappingActors);
    for (AActor *Actor : OverlappingActors)
    {
        if (debug)
        {
            UE_LOG(LogTemp, Warning, TEXT("Actor: %s"), *Actor->GetName());
        }
        // check if actor has tag Unlock tag
        if (Actor->ActorHasTag(UnlockTag) && !Actor->ActorHasTag("Grabbed"))
        {
            return Actor;
        }
    }
    return nullptr;
}

void UTriggerComponent::SetMover(UMover *MoverToSet)
{
    Mover = MoverToSet;
}
